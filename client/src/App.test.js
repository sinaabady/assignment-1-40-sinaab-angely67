import { render, screen } from '@testing-library/react';
import App from './App.js';

it("renders App without crashing", () => {
  render(<App />);
});
it("renders all the components", () => {
  render(<App />);
  expect(screen.getByText('Add to Cart')).toBeInTheDocument();
  expect(screen.getByText('Item')).toBeInTheDocument();
  expect(screen.getByText('Subtotal')).toBeInTheDocument();
  expect(screen.getByText('Discount')).toBeInTheDocument();
  expect(screen.getByText('Shipping')).toBeInTheDocument();
  expect(screen.getByText('Tax')).toBeInTheDocument();
  expect(screen.getByText('Checkout Total')).toBeInTheDocument();
});