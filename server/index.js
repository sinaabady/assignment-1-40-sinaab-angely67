// server/index.js

const express = require("express");
const path = require("path");
const mongoose = require("mongoose");
const router = require('../routes/router');

mongoose.connect(
  "mongodb://user:Abc321@cluster0-shard-00-00.7btgw.mongodb.net:27017,cluster0-shard-00-01.7btgw.mongodb.net:27017,cluster0-shard-00-02.7btgw.mongodb.net:27017/a1?ssl=true&replicaSet=atlas-dlajg5-shard-0&authSource=admin&retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
  console.log("Connected successfully");
});

const PORT = process.env.PORT || 3001;

const app = express();

app.use(express.static(path.join(__dirname, '../client/build')))

app.use('/api', router)

app.get("/api", (req, res) => {
  res.json({ message: "Hello from server!" });
});

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});

app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "https://csc301-a1-327819.nn.r.appspot.com");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

module.exports = app
