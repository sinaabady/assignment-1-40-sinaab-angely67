const express = require("express");
const DAL = require('../DAL/data_handler.js'); //To use the DAL functions
const router = express.Router()

router.get('/item/:id', DAL.getItemByID)
router.get('/items', DAL.getItems)
router.get('/discounts', DAL.getDiscounts)

module.exports = router
