# Assignment 1

## The Link
https://csc301-a1-327819.nn.r.appspot.com/ 

## CI/CD 
The CI/CD was done using GitLab.\
See the link to the GitLab repo https://gitlab.com/sinaabady/assignment-1-40-sinaab-angely67 \
When an external dev branch (ex. sina-dev) is merged to dev, auto units tests are run.\
When dev is merged to main, the app is built, and uses a Google App Engine Service account to deploy to Google App Engine which hosts the link you see above
###### The GitHub repository represents the state of the GitLab repo after pushes.
##### The GitHub repo is only updated with the dev and main branches, as they are the only relevant ones. All other test branches are contained in the GitLab and can be viewed in the link provided :)

Please go the gitlab link, under CI/CD you can view all the operations done on this repository via auto test/merge as well as automatically uploading the app image to the Google Cloud Service\
Under the ".gitlab-ci.yml" file, you can view all the configurations for these proccesses. 

## The following application was built with:
#### React <img src="https://www.freecodecamp.org/news/content/images/2021/06/Ekran-Resmi-2019-11-18-18.08.13.png" width="80" height="80" /> Node & Express <img src="https://tsh.io/wp-content/uploads/2019/05/node-js-injection_.jpg" width="80" height="80" /> MongoDB <img src="https://developer-tech.com/wp-content/uploads/sites/3/2021/02/mongodb-atlas-google-cloud-partnership-nosql-databases-integrations-2.jpg" width="80" height="80" /> Gitlab <img src="https://about.gitlab.com/images/opengraph/gitlab-blog-cover.png" width="80" height="80" /> Google Cloud <img src="https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/6f/be/1f/6fbe1feb-b1a0-b68c-1bc1-ace67b829351/contsched.ecikxjdd.png/1200x630wa.png" width="80" height="80" />

----------------------------
###### Below showcases the ability to add items to your cart.
Watch as the prices change and the total is calculated.\
It also shows the ability to add (+) and subtract (-) items that are already in the cart\
Finally it shows the ability the delete an item from your cart entirely

![Alt Text](additems.gif)

----------------------------
###### Below showcases the ability to apply a custom promocode to the checkout. 
DEMO1 is just one code\
Feel free to try out TEST123 as well :)\
Watch as the discount is applied and subtracted from the subtotal

![Alt Text](promocode.gif)

----------------------------
###### Below showcases three instances
When trying to add no item to the cart, the app catches this and sends a popup to the user informing this behaviour is not allowed\
When trying to add an item that is already in the cart, a popup shows up to inform the user no duplicate items are allowed\
Finally when click the checkout button, a popup appears giving the user a quick summary of the cart, as well as resetting the app for a new purchase

![Alt Text](popupsandcheckout.gif)
