import React from "react";

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';

import { getAccordionDetailsUtilityClass, Paper } from "@mui/material";

import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';

import Divider from '@mui/material/Divider';

import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';

import Container from '@mui/material/Container';

import IconButton from '@mui/material/IconButton';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import DeleteIcon from '@material-ui/icons/Delete';

import TextField from '@mui/material/TextField'

function createData(item, quantity, price) {
  return { item, quantity, price };
}

var itemstobuy = [
  createData('...', 1, 0.00)
];

var discountList = [
  createData('', '', 0.00)
];

var curDiscount = { name: "", discount: 0, code: "" };

function discountString() {
  if (curDiscount.discount === 0) return "";
  return curDiscount.name + "(-" + curDiscount.discount * 100 + "%)";
}

function getItemIndex(key) {
  var i = 0;
  for (var row of itemstobuy) {
    if (row.item === key) {
      return i;
    }
    i++;
  }

  return -1;
}

function getPrice(key) {
  var i = 0;
  for (var row of itemstobuy) {
    if (row.item === key) {
      return itemstobuy[i].price;
    }
    i++;
  }

  return -1;
}

var tableitems = [];

function addItem(iteminfo) {
  tableitems.push(createData(iteminfo.item, iteminfo.quantity, iteminfo.price));
}

function plusItem(key) {
  var i = 0;
  for (var row of tableitems) {
    if (row.item === key) {
      tableitems[i].quantity += 1;
      tableitems[i].price += getPrice(row.item);
      return;
    }
    i++;
  }
}

function minusItem(key) {
  var i = 0;
  for (var row of tableitems) {
    if (row.item === key) {
      tableitems[i].quantity -= 1;

      if (tableitems[i].quantity === 0) {
        tableitems.splice(i, 1);
      }
      break;
    }
    i++;
  }
}

function deleteItem(key) {
  var i = 0;
  for (var row of tableitems) {
    if (row.item === key) {
      tableitems.splice(i, 1);
      return;
    }
    i++;
  }
}

function getQuantities() {
  var qs = [];

  for (var row of tableitems) {
    qs.push(row.quantity);
  }

  return qs;
}

function getQuantIndex(key) {
  var i = 0;

  for (var row of tableitems) {
    if (row.item === key) {
      return i;
    }

    i++;
  }

  return i;
}

function getTaxRate() {
  return 0.13;
}

function getShippingRate() {
  return 10;
}

function getDiscountRate() {
  return curDiscount.discount;
}

function getSubTotal() {
  var total = 0;

  for (var row of tableitems) {
    total += row.price;
  }

  return total;
}

function getTax() {
  return getSubTotal() * getTaxRate();
}

function getShipping() {
  if (getSubTotal() === 0)
    return 0;
  else
    return getShippingRate();
}

function getDiscount() {
  return getSubTotal() * (-1 * getDiscountRate());
}

function getTotal() {
  return getSubTotal() + getDiscount() + getShipping() + getTax()
}

function App() {
  const [item, setItem] = React.useState(null);//setData
  const [inputCode, setInputCode] = React.useState(null);//setData
  const [discountApplied, setDiscountApplied] = React.useState(null);

  React.useEffect(() => {
    fetch("/api/items")
      .then((res) => res.json())
      .then((data) => {
        console.log(data.data);
        setItem(data.data);
        itemstobuy = data.data;
      });

    fetch("/api/discounts")
      .then((res) => res.json())
      .then((data) => {
        discountList = data.data;
      });
  }, []);

  const [selectitem, setSelectItem] = React.useState('');
  const handleSelect = (event) => {
    setSelectItem(event.target.value);
  };

  const [addeditem, setAddedItem] = React.useState('');

  const [quantities, setQuantities] = React.useState([]);

  return (
    <Card sx={{ maxWidth: 475, backgroundColor: "rgb(230, 242, 255)" }}>
      <CardContent>

        <Container>
          <FormControl sx={{ m: 1, minWidth: 120 }}>
            <InputLabel id="demo-simple-select-helper-label">Add</InputLabel>
            <Select
              labelId="demo-simple-select-helper-label"
              id="demo-simple-select-helper"
              value={selectitem}
              label="Add"
              onChange={handleSelect}
            >
              {itemstobuy.map((row) => (
                <MenuItem value={getItemIndex(row.item)}>
                  <ListItemText>{row.item}</ListItemText>
                  <Typography variant="body2" color="text.secondary">
                    {"$" + row.price.toFixed(2)}
                  </Typography>
                </MenuItem>
              ))}
            </Select>
            <FormHelperText>Use the dropdown to buy items :)</FormHelperText>
          </FormControl>

          <Button
            color="info"
            variant="contained"
            sx={{ marginTop: "30px", marginLeft: "30px", marginBottom: "10px", fontSize: "10px" }}
            onClick={() => {
              for (var row of tableitems) {
                if (row.item === itemstobuy[selectitem].item) {
                  alert(itemstobuy[selectitem].item + ' already added to cart');
                  return;
                }
              }

              if (selectitem === "") {
                alert("Please select an item to add");
                return;
              }

              addItem(itemstobuy[selectitem]);

              // forces updates to variables that are used in the very defintion of this row
              // sounds like a paradox but it works LOL 
              setAddedItem(itemstobuy[selectitem].item);
              setQuantities(getQuantities());

              console.log('added ' + addeditem);
              console.log('quantified ' + quantities);
            }}
          >
            Add to Cart
            <AddShoppingCartIcon />
          </Button>

        </Container>
        <Paper>
          <Table sx={{ maxWidth: 450 }} aria-label="simple table">
            <TableHead>
              <TableRow sx={{ backgroundColor: 'rgb(128, 191, 255)' }}>
                <TableCell sx={{ fontWeight: 'bold' }}>Item</TableCell>
                <TableCell sx={{ fontWeight: 'bold' }} align="right">Quantity</TableCell>
                <TableCell sx={{ fontWeight: 'bold' }} align="right">Price ($)</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {tableitems.map((row) => (
                <TableRow
                  key={row.item}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.item}
                    <IconButton
                      aria-label="subtract1"
                      sx={{ color: "rgb(57, 65, 79)" }}
                      onClick={() => {
                        deleteItem(row.item);
                        setQuantities(getQuantities()); // forces update which is technically correct but not used here for that purpose
                      }}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                  <TableCell align="right">

                    <IconButton
                      aria-label="subtract1"
                      sx={{ color: "rgb(232, 14, 36)" }}
                      onClick={() => {
                        minusItem(row.item);
                        setQuantities(getQuantities());
                      }}
                    >
                      <RemoveIcon />
                    </IconButton>

                    {getQuantities()[getQuantIndex(row.item)]}

                    <IconButton
                      aria-label="plus1"
                      sx={{ color: "rgb(39, 232, 72)" }}
                      onClick={() => {
                        plusItem(row.item);
                        setQuantities(getQuantities());
                      }}
                    >
                      <AddIcon />
                    </IconButton>

                  </TableCell>
                  <TableCell align="right">{row.price.toFixed(2)}</TableCell>
                </TableRow>
              ))}

            </TableBody>
          </Table>
        </Paper>

        <Paper sx={{ margin: "10px", backgroundColor: "#c4def6" }}>
          <Table>
            <TableRow
              key={0}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell sx={{ fontWeight: "bold" }}>{"Subtotal"}</TableCell>
              <TableCell></TableCell>
              <TableCell align="right">{"$" + getSubTotal().toFixed(2)}</TableCell>
            </TableRow>
          </Table>
        </Paper>

        <Divider
          sx={{ marginBottom: "20px", marginTop: "20px" }}
          color="#1a237e"
          orientation="horizontal" f
          lexItem />


        <Paper sx={{ margin: "10px" }}>
          <Table>
            <TableRow>
              <TableCell sx={{ fontWeight: "bold" }}>{"Discount"}</TableCell>
              <TableCell colSpan={2} align="right" sx={{ color: "green", fontWeight: "bold" }}></TableCell>
            </TableRow>
            <TableRow>
              <TableCell><TextField id="discount-code" refs="discountcode" label="Enter Code" variant="outlined" onKeyPress={(ev) => {
                console.log(`Pressed keyCode ${ev.key}`);
                if (ev.key === 'Enter') {
                  var ticket = discountList.find(x => x.code === inputCode);
                  if (!ticket) {
                    alert("Invalid code!");
                  }
                  else {
                    alert("Discount code applied");
                    curDiscount = ticket;
                    setDiscountApplied(curDiscount);
                  }
                  ev.preventDefault();
                }
              }}

                onChange={(e) => { setInputCode(e.target.value) }} /><InputLabel>{"\n"}</InputLabel> You can only use one code</TableCell>
              <TableCell colSpan={2}></TableCell>
            </TableRow>
            <TableRow
              key={0}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
              <TableCell colSpan={2}>{discountString()}</TableCell>
              <TableCell align="right" sx={{ color: "green", fontWeight: "bold" }}>{"$" + getDiscount().toFixed(2)}</TableCell>
            </TableRow>
          </Table>
        </Paper>


        <Paper sx={{ margin: "10px" }}>
          <Table>
            <TableRow
              key={0}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell sx={{ fontWeight: "bold" }}>{"Shipping"}</TableCell>
              <TableCell></TableCell>
              <TableCell align="right">{"$" + getShipping().toFixed(2)}</TableCell>
            </TableRow>
          </Table>
        </Paper>

        <Paper sx={{ margin: "10px" }}>
          <Table>
            <TableRow
              key={0}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell sx={{ fontWeight: "bold" }}>{"Tax"}</TableCell>
              <TableCell></TableCell>
              <TableCell align="right">{"$" + getTax().toFixed(2)}</TableCell>
            </TableRow>
          </Table>
        </Paper>

        <Divider sx={{ marginBottom: "20px", marginTop: "20px" }} color="#1a237e" orientation="horizontal" flexItem />

        <Paper sx={{ margin: "10px", backgroundColor: 'rgb(128, 191, 255)' }}>
          <Table>
            <TableRow
              key={0}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell sx={{ fontWeight: "bold" }}>{"Checkout Total"}</TableCell>
              <TableCell></TableCell>
              <TableCell align="right">{"$" + getTotal().toFixed(2)}</TableCell>
            </TableRow>
          </Table>
        </Paper>

        <Stack direction="row" sx={{ marginLeft: "170px" }}>
          <Button
            variant="outlined"
            color="primary"
            onClick={() => {
              var t = "$" + getTotal().toFixed(2);
              var s = "";
              for (var r of tableitems) {
                s += r.item + "[" + r.quantity + "]" + "        " + "$" + r.price.toFixed(2) + "\n";
              }
              tableitems = [];
              setQuantities(getQuantities());
              setDiscountApplied('');
              setInputCode('');
              alert("Thank you for purchasing\n" + s + "\nFor " + t + ".\n\n" + "Please come again :D");
            }}>
            Checkout
          </Button>
        </Stack>

      </CardContent>
    </Card>
  );
}

export default App;