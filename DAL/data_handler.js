const Schemas = require('../models/model.js');

getItems = async (req, res) => {
    const Item = Schemas.Item;
    await Item.find({}, (err, items) => {
        if (err) {return res.status(400).json({ success: false, error: err }) }
        if (!items.length) {
            return res
                .status(404)
                .json({ success: false, error: 'No data' })
        }
        var ret = [];
        items.forEach(x => {
            ret.push({item:x.name, quantity:1, price:x.price});
        });

        return res.status(200).json({ success: true, data: ret })
    }).catch(err => console.log(err))
}


getItemByID = async (req, res) => {
    const Item = Schemas.Item;
    await Item.findOne({ _id: req.params.id }, (err, item) => {
        if (err) {return res.status(400).json({ success: false, error: err })}
  
        if (!item) {
            return res
                .status(404)
                .json({ success: false, error: 'Item not found' })
        }
        var ret = {item:item.name, quantity:1, price:item.price};
        return res.status(200).json({ success: true, data: ret })
    }).catch(err => console.log(err))
  }

  getDiscounts = async (req, res) => {
    const Discount = Schemas.Discount;
    await Discount.find({}, (err, discounts) => {
        if (err) { return res.status(400).json({ success: false, error: err })}
        if (!discounts.length) {
            return res
                .status(404)
                .json({ success: false, error: 'No data' })
        }
        var ret = [];
        discounts.forEach(x => {
            ret.push({name:x.name, code:x.code, discount:x.discount});
        });
        return res.status(200).json({ success: true, data: ret })
    }).catch(err => console.log(err))
}

module.exports.getItems = getItems;
module.exports.getDiscounts = getDiscounts;
module.exports.getItemByID = getItemByID;

