const mongoose = require('mongoose')

var ItemSchema = mongoose.Schema({
    name: {type:String, required:true},
    price: {type:Number, required:true}
  }, { collection: 'items' });
  
var Item = mongoose.model('Item', ItemSchema, 'items');

var DiscountSchema = mongoose.Schema({
    name: {type:String, required:true},
    code: {type:String, required:true},
    discount: {type:Number, required:true}
  }, { collection: 'discounts' });
  
var Discount = mongoose.model('Discount', DiscountSchema, 'discounts');

const mySchemas = {'Item':Item, 'Discount':Discount};

module.exports = mySchemas;